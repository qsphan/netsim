package netsim.protocol;

public class Protocol {

	public static final int MIN = 0;
	public static final int UDP = 0;
	public static final int ICMP = 1;
	public static final int ANY = 2;
	// ANY is for firewall, not for a particular package
	// So the maximum is still 1, this is for symbolic execution
	public static final int MAX = 1;
	
	public static String[] name = {"UDP", "ICMP", "ANY"
	};

	public static int getCode(String protocol) {
		protocol = protocol.trim().toUpperCase();
		switch (protocol) {
		case "ANY":
			return ANY;
		case "UDP":
			return UDP;
		case "ICMP":
			return ICMP;
		default:
			assert false;
		}
		// error, should never be reached
		return -1;
	}

	public static String getName(int protocol) {
		return name[protocol];
	}
}
