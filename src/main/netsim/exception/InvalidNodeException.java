package netsim.exception;

public class InvalidNodeException extends Exception{

	private static final long serialVersionUID = 2487904376766264090L;

	public InvalidNodeException(int id) {
		super("Invalid node " + id );
	}
}
