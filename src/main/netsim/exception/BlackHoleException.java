package netsim.exception;

import netsim.routing.Node;

public class BlackHoleException extends Exception{

	private static final long serialVersionUID = 2359363596604848264L;

	public BlackHoleException (Node n){
		super("Black hole at node " + n);
	}

}
