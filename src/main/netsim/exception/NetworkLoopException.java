package netsim.exception;

public class NetworkLoopException extends Exception {
	
	private static final long serialVersionUID = 728168383759749953L;

	public NetworkLoopException() {
		super("There is a loop in the network");
	}
}
