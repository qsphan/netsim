package netsim.exception;

import netsim.routing.Node;

public class NoStandardRuleException extends Exception {

	private static final long serialVersionUID = 7687670379045199153L;

	public NoStandardRuleException(Node n) {
		super("No rule in the firewall of node " + n + " covers the IP of this message");
	}
}
