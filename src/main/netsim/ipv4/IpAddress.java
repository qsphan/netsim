package netsim.ipv4;

import java.net.UnknownHostException;

/*
 * An IP address
 */
public class IpAddress {
	
	public static final int NUM_OF_BYTES = 4;

	protected int[] byteArray = new int[NUM_OF_BYTES];
	protected String toStringLazy = null;
	
	public IpAddress() {}
	
	public IpAddress(int[] byteArray) {
		this.byteArray = byteArray;
	}
	
	public IpAddress(String host) throws UnknownHostException {
		setHostByName(host);
	}

	// example of host: 10.91.114.0
	public void setHostByName(String host) throws UnknownHostException {
		toStringLazy = host;
		// cannot use this with JPF/SPF
		// byte[] other = InetAddress.getByName(host).getAddress();
		String[] tokens = host.split("\\.");
		for(int i = 0; i < NUM_OF_BYTES; ++i) {
			byteArray[i] = Integer.parseInt(tokens[i]);
		}
	}
	
	public int[] getBytes() {
		return byteArray;
	}
	
	public static void main (String[] args) throws Exception{
		IpAddress ip = new IpAddress();
		ip.setHostByName("10.91.114.0");
		for(int b : ip.getBytes()) {
			System.out.print(b + " ");
		}
	}
	
	@Override
	public String toString() {
		return toStringLazy;
	}
}
