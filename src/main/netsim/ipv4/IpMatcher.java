package netsim.ipv4;

import java.net.UnknownHostException;

public class IpMatcher extends IpAddress{

	// The number of bits to be matched
	protected int numOfBits = 0;
	
	// example of string rule: 10.91.114.0/25
	public IpMatcher(String rule) throws UnknownHostException {
		int pos = rule.indexOf("/");
		setHostByName(rule.substring(0, pos));
		numOfBits = Integer.parseInt(rule.substring(pos+1));
	}
	
	public int getNumOfBits() {
		return numOfBits;
	}
	
	public static void main (String[] args) throws UnknownHostException{
		IpMatcher matcher = new IpMatcher("10.91.114.0/25");
		for(int b : matcher.getBytes()) {
			System.out.print(b +  " ");
		}
		System.out.println("\nNumber of bits is " + matcher.numOfBits);
		System.out.println("\nSegment is " + matcher.numOfBits / 8);
		System.out.println("\nOffset is " + matcher.numOfBits % 8);
		if(matcher.match(new IpAddress("10.91.114.2"))) {
			System.out.println("Matched");
		} else {
			System.out.println("Not matched");
		}
	}
	
	public boolean match(IpAddress ip) {
		if(numOfBits == 0) {
			return true;
		}
		// position = segment * 8 + offset
		int segment = numOfBits / 8;
		int offset = numOfBits % 8;

		boolean cornerCase = (offset == 0) || (offset == 32);
		// OK, we have all value, now start matching
		int[] other = ip.getBytes();
		int i;
		for (i = 0; i < segment; ++i) {
			if (other[i] != byteArray[i]) {
				return false;
			} else {
				// clear the symbolic variables if they are used
				other[i] = byteArray[i];
			}
		}
		if(cornerCase) {
			return true;
		}
		
		int mask = 1;
		for(i = 1; i < offset; ++i) {
			mask |= mask << 1;
		}
		int val = mask & byteArray[segment];
		/* get min and max value of the last byte to match from offset

		int min = byteArray[segment] & mask;
		// flip all the bits, but only get first 8 bits
		mask = ~mask & 255; 
		int max = min | mask;
		//*/
		return (other[segment] & mask) == val;
	}
}
