package netsim.builder.xml;

import java.net.UnknownHostException;

import org.xml.sax.SAXException;

import netsim.Network;
import netsim.routing.Node;
import netsim.routing.rule.FirewallRule;

public class XmlFirewallBuilder extends XmlRuleBuilder {

	protected String source = null;
	protected String protocol = null;
	protected String action = null;

	public XmlFirewallBuilder(Network net) {
		super(net);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		qName = qName.toLowerCase();
		switch(qName) {
		case "id":
			int id = Integer.parseInt(sb.toString());
			node = net.get(id);
			if (node == null) {
				node = new Node(id);
				net.addNode(node);
			}
			break;
		case "source":
			source = sb.toString();
			break;
		case "destination":
			destination = sb.toString();
			break;		
		case "protocol":
			protocol = sb.toString();
			break;	
		case "action":
			action = sb.toString();
			break;	
		case "rule":
			// at this point, we should have all source, destination, protocol and action
			try {
				FirewallRule rule = new FirewallRule(source, destination, protocol, action);
				node.addFirewallRule(rule);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
	}

}
