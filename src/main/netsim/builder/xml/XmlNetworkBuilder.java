package netsim.builder.xml;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import netsim.Network;

public class XmlNetworkBuilder {
	
	public Network build(String routingPath, String firewallPath) throws Exception {
		Network net = new Network();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		DefaultHandler routingHandler = new XmlRouterBuilder(net); 
		saxParser.parse(routingPath, routingHandler);
		
		DefaultHandler firewallHandler = new XmlFirewallBuilder(net); 
		saxParser.parse(firewallPath, firewallHandler);
		return net;
	}

}
