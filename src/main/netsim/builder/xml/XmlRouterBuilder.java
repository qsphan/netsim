package netsim.builder.xml;

import java.net.UnknownHostException;

import org.xml.sax.SAXException;

import netsim.Network;
import netsim.routing.Node;
import netsim.routing.rule.RoutingRule;

public class XmlRouterBuilder extends XmlRuleBuilder {

	protected String next = null;

	public XmlRouterBuilder(Network net) {
		super(net);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		qName = qName.toLowerCase();
		switch (qName) {
		case "id":
			int id = Integer.parseInt(sb.toString());
			node = net.get(id);
			if (node == null) {
				node = new Node(id);
				net.addNode(node);
			}
			break;
		case "destination":
			destination = sb.toString();
			break;
		case "next":
			next = sb.toString();
			break;
		case "rule":
			// at this point, we should have destination and next
			try {
				RoutingRule rule = new RoutingRule(destination, next);
				node.addRoutingRule(rule);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			break;
		}
	}
}
