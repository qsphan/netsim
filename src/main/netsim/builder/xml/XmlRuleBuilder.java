package netsim.builder.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import netsim.Network;
import netsim.routing.Node;

public class XmlRuleBuilder extends DefaultHandler {

	protected final int ELEM_ID = 0;
	protected final int ELEM_SOURCE = 1;
	protected final int ELEM_DESTINATION = 2;
	protected final int ELEM_PROTOCOL = 3;
	protected final int ELEM_ACTION = 4;
	protected final int ELEM_NEXT = 5;
	protected final int ELEM_RULE = 6;
	
	protected String destination;

	protected int element = -1; // invalid element
	protected StringBuilder sb = null;
	
	protected Network net = null;
	protected Node node = null;
	
	protected boolean read = false;
	
	public XmlRuleBuilder(Network net) {
		this.net = net;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		qName = qName.toLowerCase();
		if(qName.equals("id") || qName.equals("source")||  qName.equals("destination") 
				|| qName.equals("protocol") || qName.equals("action") || qName.equals("next")){
			sb = new StringBuilder();
			read = true;
		} else {
			read = false;
		}
	}
	
	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		// Note that this method can be called several times for one text node
		// So it is mandatory to use string builder to append all the results of the calls
		if(read) {
			sb.append(ch, start, length);
		}
	}
}
