package netsim.builder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import netsim.Network;
import netsim.routing.Node;

public class RuleBuilder {

	public static final String SEPARATOR = ",";

	protected int previousID = Node.INVAlID_ID;
	protected Node node = null;
	protected Network net = null;
	
	public RuleBuilder(Network net) {
		this.net = net;
	}

	public void parse(String fileName) throws IOException {

		FileInputStream fstream = new FileInputStream(fileName);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		// Read File Line By Line
		while ((strLine = br.readLine()) != null) {
			String tokens[] = strLine.split(SEPARATOR);
			processTokens(tokens);
		}
		// Close the input stream
		br.close();
	}

	protected void processTokens(String tokens[]) throws UnknownHostException {
		int id = Integer.parseInt(tokens[0]);
		if(previousID == Node.INVAlID_ID || id != previousID) {
			// new node
			node = net.get(id);
			if (node == null) {
				node = new Node(id);
				net.addNode(node);
			}
			previousID = id;
		}
	}

}
