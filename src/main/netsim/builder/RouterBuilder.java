package netsim.builder;

import java.net.UnknownHostException;

import netsim.Network;
import netsim.routing.rule.RoutingRule;

public class RouterBuilder extends RuleBuilder{

	public RouterBuilder(Network net) {
		super(net);
	}

	@Override
	protected void processTokens(String[] tokens) throws UnknownHostException {
		super.processTokens(tokens);
		RoutingRule rule = new RoutingRule(tokens[1], tokens[2]);
		node.addRoutingRule(rule);
	}
}
