package netsim.builder;

import netsim.Network;

public class NetworkBuilder {

	public Network build(String routingPath, String firewallPath) throws Exception {
		Network net = new Network();
		RouterBuilder router = new RouterBuilder(net);
		router.parse(routingPath);
		FirewallBuilder firewall = new FirewallBuilder(net);
		firewall.parse(firewallPath);
		return net;
	}
}
