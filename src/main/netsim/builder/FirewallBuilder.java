package netsim.builder;

import java.net.UnknownHostException;

import netsim.Network;
import netsim.routing.rule.FirewallRule;

public class FirewallBuilder extends RuleBuilder {

	public FirewallBuilder(Network net) {
		super(net);
	}

	@Override
	protected void processTokens(String[] tokens) throws UnknownHostException {
		super.processTokens(tokens);
		FirewallRule rule = new FirewallRule(tokens[1], tokens[2], tokens[3], tokens[4]);
		node.addFirewallRule(rule);
	}

}
