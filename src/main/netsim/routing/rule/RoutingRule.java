package netsim.routing.rule;

import java.net.UnknownHostException;

import netsim.routing.Node;
import netsim.routing.Package;

public class RoutingRule extends Rule {
	
	protected int nextHop = Node.INVAlID_ID;
	
	public RoutingRule(String destination, String nextHop) throws UnknownHostException {
		super(destination);
		lazyToString = destination + "  " + nextHop;
		this.nextHop = Integer.parseInt(nextHop);
	}

	public int getNextHop() {
		return nextHop;
	}

	@Override
	public boolean match(Package p) {
		return destination.match(p.getDestination());
	}
	
	
	
}
