package netsim.routing.rule;

import java.net.UnknownHostException;

import netsim.ipv4.IpMatcher;
import netsim.routing.Package;

public abstract class Rule {

	protected IpMatcher destination = null;
	protected String lazyToString = null;

	public Rule(String destination) throws UnknownHostException {
		this.destination = new IpMatcher(destination);
	}

	public abstract boolean match(Package p);
	
	@Override
	public String toString() {
		return lazyToString;
	}
}
