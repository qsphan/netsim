package netsim.routing.rule;

import java.net.UnknownHostException;

import netsim.ipv4.IpMatcher;
import netsim.protocol.Protocol;
import netsim.routing.Package;

public class FirewallRule extends Rule {

	protected IpMatcher source = null;
	protected int protocol = -1;
	protected boolean negation = false;
	public static final boolean ACCEPT = true;
	public static final boolean DROP = false;
	protected boolean action = DROP;

	public FirewallRule(String source, String destination, String protocol, String action) throws UnknownHostException {
		super(destination);
		lazyToString = source + " | " + destination + " | " + protocol + " | " + action; 
		this.source = new IpMatcher(source);
		this.protocol = getProtocol(protocol);
		switch (action) {
		case "ACCEPT":
			this.action = ACCEPT;
			break;
		case "DROP":
			this.action = DROP;
			break;
		default:
			assert false;
		}
	}

	@Override
	public boolean match(Package p) {
		int target = p.getProtocol();
		// TODO: change this to XOR???
		boolean matched = false;
		if(protocol == Protocol.ANY) {
			matched = true;
		} else {
			if(negation) {
				matched = (protocol != target);
			} else {
				matched = protocol == target;
			}
		}
		if(matched == false) {
			return false;
		}
		return source.match(p.getSource()) && destination.match(p.getDestination());
	}

	public boolean getAction() {
		return action;
	}

	private int getProtocol(String protocol) {
		if (protocol.charAt(0) == '!') {
			negation = true;
			protocol = protocol.substring(1);
		}
		return Protocol.getCode(protocol);
	}
}
