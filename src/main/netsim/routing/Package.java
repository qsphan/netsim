package netsim.routing;

import java.net.UnknownHostException;

import netsim.ipv4.IpAddress;
import netsim.protocol.Protocol;

public class Package {

	protected IpAddress source = new IpAddress();
	protected IpAddress destination = new IpAddress();
	protected int protocol = -1;
	
	public Package(String source, String destination, String protocol) throws UnknownHostException {
		this.source.setHostByName(source);
		this.destination.setHostByName(destination);
		this.protocol = Protocol.getCode(protocol);
	}
	
	public Package(IpAddress source, IpAddress destination, int protocol) {
		this.source = source;
		this.destination = destination;
		this.protocol = protocol;
	}
	
	public IpAddress getSource() {
		return source;
	}
	
	public IpAddress getDestination() {
		return destination;
	}
	
	public int getProtocol() {
		return protocol;
	}
	
	@Override public String toString() {
		return source +  " -> " + destination + " by " + Protocol.getName(protocol);
	}
}
