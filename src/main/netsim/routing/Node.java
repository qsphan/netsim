package netsim.routing;

import java.util.ArrayList;

import netsim.exception.BlackHoleException;
import netsim.exception.NoStandardRuleException;
import netsim.routing.rule.FirewallRule;
import netsim.routing.rule.RoutingRule;

public class Node {

	public static final int INVAlID_ID = -1;
	public static final int DROPPED_ID = -2;
	
	// -1 is an invalid id
	protected Integer id = INVAlID_ID;

	// the order of these rules is important
	// A rule with smaller index will have higher priority
	protected ArrayList<RoutingRule> routingTable = new ArrayList<RoutingRule>();
	protected ArrayList<FirewallRule> firewallConfig = null;

	public Node(int id) {
		assert id >= 0;
		this.id = id;
	}
	
	public Integer getID() {
		return id;
	}
	
	public void addRoutingRule(RoutingRule rule) {
		routingTable.add(rule);
	}
	
	public void addFirewallRule(FirewallRule rule) {
		if(firewallConfig == null) {
			firewallConfig = new ArrayList<FirewallRule>();
		}
		firewallConfig.add(rule);
	}

	// process a package, returning the next node
	public int process(Package p) throws NoStandardRuleException, BlackHoleException {
		if (filter(p) == FirewallRule.DROP) {
			return Node.DROPPED_ID;
		}
		// OK, the firewall lets the package go through. Forward it to the next node
		return forward(p);
	}

	/*
	 * Filter the package with the firewall
	 */
	protected boolean filter(Package p) throws NoStandardRuleException {
		if (firewallConfig == null) {
			return true;
		}
		for (FirewallRule rule : firewallConfig) {
			if (rule.match(p)) {
				// first rules in the list have higher priority
				System.out.println("Rule matched: |" + rule);
				return rule.getAction();
			}
		}
		// There is no rule in the firewall that can be applied to this package.
		throw new NoStandardRuleException(this);
		// TODO: or should we just let it go ???
	}
	
	protected int forward(Package p) throws BlackHoleException {
		for (RoutingRule rule : routingTable) {
			if (rule.match(p)) {
				return rule.getNextHop();
			}
		}
		// There is no routing rule for this package
		throw new BlackHoleException(this);
	}
	
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
	
	@Override
	public String toString() {
		return id.toString();
	}
	
	public void printConfig() {
		System.out.println("Routing table:");
		//*
		routingTable.forEach(routingRule->{
			System.out.println(routingRule.toString());
		});
		//*/
		//*
		System.out.println("Firewall Configuration:");
		firewallConfig.forEach(firewallRule->{
			System.out.println(firewallRule.toString());
		});
		//*/
	}
}
