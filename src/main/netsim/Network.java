package netsim;

import java.util.HashMap;
import java.util.HashSet;

import netsim.exception.InvalidNodeException;
import netsim.exception.NetworkLoopException;
import netsim.routing.Node;
import netsim.routing.Package;

public class Network {

	HashMap<Integer,Node> nodes = new HashMap<Integer,Node>();
	
	public boolean contains(int id) {
		return nodes.containsKey(id);
	}
	
	public Node addNode(Node n) {
		return nodes.put(n.getID(),n);
	}
	
	public Node get(int id) {
		return nodes.get(id);
	}
	
	public void process(Package p, int n) throws Exception {
		// to detect loop
		HashSet<Node> visited = new HashSet<Node>();
		Node current = nodes.get(n);
		if(current == null) {
			throw new InvalidNodeException(n);
		}
		System.out.println("Package  " + p);
		while(true) {
			if(!visited.add(current)) {
				throw new NetworkLoopException();
			}
			System.out.println("Process package at node " + current);
			int next = current.process(p);
			if(next == Node.DROPPED_ID) {
				System.out.println("Package is dropped by firewall at node " + current);
				return;
			}
			current = nodes.get(next);
			if(current == null) {
				throw new InvalidNodeException(next);
			}
			// TODO: stopping condition
		}
	}
	
	/*
	 * Print routing tables and firewall configuration for debug
	 */
	public void printConfig() {
		nodes.forEach((K,V)->{
			System.out.println("\n\nNode " + K);
			V.printConfig();
		});
	}
}
