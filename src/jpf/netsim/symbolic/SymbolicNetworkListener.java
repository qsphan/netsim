package netsim.symbolic;

import java.util.Map;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.PropertyListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.symbc.numeric.PCChoiceGenerator;
import gov.nasa.jpf.symbc.numeric.PathCondition;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.VM;
import netsim.protocol.Protocol;

public class SymbolicNetworkListener extends PropertyListenerAdapter {

	public SymbolicNetworkListener(Config conf, JPF jpf) {
		jpf.getReporter().getPublishers().clear();
	}

	@Override
	public void propertyViolated(Search search) {

		VM vm = search.getVM();

		ChoiceGenerator<?> cg = vm.getChoiceGenerator();
		if (!(cg instanceof PCChoiceGenerator)) {
			ChoiceGenerator<?> prev_cg = cg.getPreviousChoiceGenerator();
			while (!((prev_cg == null) || (prev_cg instanceof PCChoiceGenerator))) {
				prev_cg = prev_cg.getPreviousChoiceGenerator();
			}
			cg = prev_cg;
		}
		if ((cg instanceof PCChoiceGenerator) && ((PCChoiceGenerator) cg).getCurrentPC() != null) {
			PathCondition pc = ((PCChoiceGenerator) cg).getCurrentPC();
			Map<String, Object> val = pc.solveWithValuation();
			System.out.println("\nPackage that triggers the exception: " + parseModel(val));
			System.out.println(search.getLastError().getDetails());
		}
	}
	
	protected String parseModel(Map<String, Object> val) {
		StringBuilder sb = new StringBuilder();
		sb.append(val.get("source_0")+".");
		sb.append(val.get("source_1")+".");
		sb.append(val.get("source_2")+".");
		sb.append(val.get("source_3")+" | ");
		sb.append(val.get("destination_0")+".");
		sb.append(val.get("destination_1")+".");
		sb.append(val.get("destination_2")+".");
		sb.append(val.get("destination_3")+" | ");
		sb.append(Protocol.getName(((Long)val.get("protocol")).intValue()));
		return sb.toString();
	}
}
