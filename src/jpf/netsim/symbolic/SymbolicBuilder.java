package netsim.symbolic;

import gov.nasa.jpf.symbc.Debug;
import netsim.ipv4.IpAddress;
import netsim.protocol.Protocol;
import netsim.routing.Package;

public class SymbolicBuilder {

	public static final int NUMBER_OF_BYTES = 4;
	
	public static IpAddress makeSymbolicIpAddress(String name) {
		int[] byteArray = new int[NUMBER_OF_BYTES];
		for(int i = 0; i < NUMBER_OF_BYTES; ++i) {
			byteArray[i] = Debug.makeSymbolicInteger(name + "_" + i); 
		}
		return new IpAddress(byteArray);
	}
	
	public static Package makeSymbolicPackage() {
		IpAddress source = makeSymbolicIpAddress("source");
		IpAddress destination = makeSymbolicIpAddress("destination");
		int protocol = Debug.makeSymbolicInteger("protocol");
		Debug.assume(protocol <= Protocol.MAX && protocol >= Protocol.MIN);
		return new Package(source, destination, protocol);
	}
}
