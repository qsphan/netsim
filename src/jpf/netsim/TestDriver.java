package netsim;

import netsim.builder.NetworkBuilder;
import netsim.builder.xml.XmlNetworkBuilder;
import netsim.routing.Package;
import netsim.symbolic.SymbolicBuilder;

public class TestDriver {
	
	public static boolean DEBUG = false;

	public static void main(String[] args) throws Exception{
		testBuilder();
		// testXmlBuilder();
	}
	
	public static void testXmlBuilder() throws Exception {
		XmlNetworkBuilder builder = new XmlNetworkBuilder();
		Network net = builder.build("data/xml/routing.xml", "data/xml/firewall.xml");
		if(DEBUG) {
			net.printConfig();
			System.out.println();
		}
		Package p = new Package("0.0.0.64","0.0.0.8","UDP");
		// p = SymbolicBuilder.makeSymbolicPackage();
		net.process(p, 0);
	}
	
	public static void testBuilder() throws Exception {
		NetworkBuilder builder = new NetworkBuilder();
		Network net = builder.build("data/routing.csv", "data/firewall.csv");
		if(DEBUG) {
			net.printConfig();
			System.out.println();
		}
		Package p = null;
		p = new Package("0.0.0.0","0.0.0.8","UDP");
		p = SymbolicBuilder.makeSymbolicPackage();
		net.process(p, 0);
	}
}
